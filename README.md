# Readme

This ansible role mounts the given hcloud volume to the given mount path by adding it to fstab.

**On changes this role will reboot your server.**

## Required vars

```ansible
volume_linux_device: "/dev/disk/by-id/scsi-0HC_Volume_12345678"
volume_mount_path: "/var/hcloudvol"
```

## Optional vars with default value

```ansible
volume_fstype: "ext4"
```
